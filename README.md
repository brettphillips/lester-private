# Jon Lester Heatmap

Heatmap of Jon Lester pitches off available pitch data, all rendered in the browser. Optional filters can be applied.

**Added March 2018**
Using Vue routing, filtered URL is now saved as a query to the URL. This allows it to be shared or bookmarked and return to the same location.

## Build Setup

You must have Node >=v5.10 and npm >=v2.5.1 installed. To build and run locally after pulling down the code, run the follow two commands. This will open up the heatmap page in your browser. 

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

## Screenshots

![All pitches](img/lester-allpitches.png "All pitches")

![Filtered pitch](img/lester-filtered.png "Filtered pitch")

![Filter options](img/lester-filters.png "Filter options")

![Filter page loaded](img/filtered-url.png "Filtered URL page")


# Technical Details

## [Plotly](https://plot.ly/javascript/)

I used Plotly to create the heatmap. It is a great library for building graphs and has [decent documentation](https://plot.ly/javascript/reference/#histogram2dcontour). Plotly takes care of calculating the density based off the x and y values, which is nice. Parameters can be adjusted when creating the graph if a more precise calculation is desired. I felt like the default values were ok for this exercise.

## [VueJS](https://vuejs.org/)

Lightweight JavaScript framework that has the ability to extend, making for a robust tool to developing web apps. 

## [Vue2 Autocomplete](https://github.com/BosNaufal/vue2-autocomplete)

I discovered this autocomplete Vue component someone built that worked for searching text. Problem using it is that the names searched needed to returned via an AJAX call. For this project, I just wanted to use a static set of batters retrieved on page load. I modified the component to accept a static object of data to be filtered.

## [Bulma](http://bulma.io/)

A modular, responsive CSS framework that styles most the page. 

# Going further

- At this moment, searching for player name does not work on a mobile device. That can be fixed but I was concerned about other things at the time.
- Write tests! Right now everything is done as UAT check. This is not the way I like to code but I wanted to get something out.
- Develop API. I almost did this as I used a Python script to create a static DB in Sqlite to query the data. I would like to create an API to return only the necessary data to render the graph, rather than relying on the browser to do the calculations.