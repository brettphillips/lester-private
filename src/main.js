import Vue from 'vue'
import VueRouter from 'vue-router'
import Heatmap from './Heatmap.vue'

Vue.use(VueRouter);
const router = new VueRouter({
  mode: 'history'
});
new Vue({
  el: '#app',
  render: h => h(Heatmap),
  router
});
